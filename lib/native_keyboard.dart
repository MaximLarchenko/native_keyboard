import 'dart:async';

import 'package:flutter/services.dart';

class NativeKeyboard {
  static const MethodChannel _channel = const MethodChannel('native_keyboard');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  Future<String> checkOs() async {
    String myResult = "";
    try {
      myResult = await _channel.invokeMethod("checkOS", <String, dynamic>{
        'param1': "hello",
      });
    } catch (e) {
      print("exception: $e");
    }
    return myResult;
  }

  Future<String> fireTvKeyboardInput() async {
    String myResult = "";
    try {
      myResult = await _channel.invokeMethod("fireKeyBoard", <String, dynamic>{
        'param1': "hello",
      });
    } catch (e) {
      print("exception: $e");
    }
    return myResult;
  }
}
