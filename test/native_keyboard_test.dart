import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:native_keyboard/native_keyboard.dart';

void main() {
  const MethodChannel channel = MethodChannel('native_keyboard');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await NativeKeyboard.platformVersion, '42');
  });
}
