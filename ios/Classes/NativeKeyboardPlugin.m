#import "NativeKeyboardPlugin.h"
#if __has_include(<native_keyboard/native_keyboard-Swift.h>)
#import <native_keyboard/native_keyboard-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "native_keyboard-Swift.h"
#endif

@implementation NativeKeyboardPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftNativeKeyboardPlugin registerWithRegistrar:registrar];
}
@end
