package com.example.native_keyboard

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.text.InputType
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** NativeKeyboardPlugin */
public class NativeKeyboardPlugin : FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel
    var context: Activity? = null


    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        final MethodChannel channel = new MethodChannel (registrar.messenger(), "native_keyboard");
        channel.setMethodCallHandler(new NativeKeyboardPlugin (registrar.activity(), channel));

    }

    fun NativeKeyboardPlugin(activity: Activity?, methodChannel: MethodChannel) {
        context = activity
        channel = methodChannel
        channel.setMethodCallHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        if (call.method == "getPlatformVersion") {
            result.success("Android ${android.os.Build.VERSION.RELEASE}")
        }
        if (call.method == "checkOS") {
            val operatingSystem = android.os.Build.MANUFACTURER + "- " + android.os.Build.MODEL
            result.success(operatingSystem)
        }

        if (call.method == "fireKeyBoard") {
            val alert = AlertDialog.Builder(context)
            alert.setMessage("Search")
            // Set an EditText view to get user input
            val input = EditText(context)
            input.hint = "Enter Text"
            input.inputType = InputType.TYPE_CLASS_TEXT
            alert.setView(input)
            input.setOnKeyListener { view, keyCode, keyEvent ->
                if (keyCode == 66) {
                    val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(input.windowToken, 0)
                }
                false
            }
            alert.setPositiveButton("Ok") { dialog, whichButton ->
                result.success(input.text.toString());
            }
            alert.setNegativeButton("Cancel") { dialog, whichButton ->
                // Canceled.
            }
            alert.show()

        } else {
            result.notImplemented()
        }
    }

    fun showDialog() {

    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}
